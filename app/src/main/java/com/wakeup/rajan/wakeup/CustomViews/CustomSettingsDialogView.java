package com.wakeup.rajan.wakeup.CustomViews;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.wakeup.rajan.wakeup.Functions.SessionManager;
import com.wakeup.rajan.wakeup.R;

public class CustomSettingsDialogView
{

        RadioButton autoStartUpOff,autoStartUpOn,mediaSelected,ringSelected,alarmSelected;

        RadioGroup tune_title,volumeOptions,autoStartupOptions;

        MediaPlayer mediaPlayer;

        TextView setAlarmButton;


    String setAlarmKeySet="SETALARMRINGTONE";
    String getalarmringtone="GETALARMRINGTONE";
    String setVolumeOutputType="SETVOLUMETYPE";
    String setAutoStartUP="SETAUTOSTARTUP";

    public void showDialog(final Activity activity, String msg, final SessionManager sessionManager,MediaPlayer mp){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_settings_dialog_view);

            autoStartUpOff=(RadioButton)dialog.findViewById(R.id.autoStartOff);
            autoStartUpOn=(RadioButton)dialog.findViewById(R.id.autoStartOn);
            tune_title=(RadioGroup)dialog.findViewById(R.id.tune_title);
            autoStartupOptions=(RadioGroup)dialog.findViewById(R.id.autostart_option);
            volumeOptions=(RadioGroup)dialog.findViewById(R.id.volumeOptions);
            mediaSelected=(RadioButton)dialog.findViewById(R.id.mediaSelected);
            ringSelected=(RadioButton)dialog.findViewById(R.id.ringSelected);
            alarmSelected=(RadioButton)dialog.findViewById(R.id.alarmSelected);

            setAlarmButton=(TextView)dialog.findViewById(R.id.setAlarmButton);

            mediaPlayer=mp;



            //setting the ringtone selected on the dialog when popped
            if (sessionManager.getValue(setAlarmKeySet)!=null)
                tune_title.check(Integer.parseInt(sessionManager.getValue(setAlarmKeySet)));
//            else
                //Toast.makeText(activity, "its empty" + sessionManager.getValue(setAlarmKeySet), Toast.LENGTH_SHORT).show();

            //setting the volume type selected on the dialog when popped
            if (sessionManager.getValue(setVolumeOutputType)!=null) {
                if(sessionManager.getValue(setVolumeOutputType).equals(String.valueOf(AudioManager.STREAM_MUSIC)))
                    volumeOptions.check(R.id.mediaSelected);
                else if(sessionManager.getValue(setVolumeOutputType).equals(String.valueOf(AudioManager.STREAM_RING)))
                    volumeOptions.check(R.id.ringSelected);
                else if(sessionManager.getValue(setVolumeOutputType).equals(String.valueOf(AudioManager.STREAM_ALARM)))
                    volumeOptions.check(R.id.alarmSelected);
            }
            else{}


            //autoStartUp initialize
        if(sessionManager.getValue(setAutoStartUP)!=null && sessionManager.getValue(setAutoStartUP).equals("YES"))
        {
            autoStartupOptions.check(R.id.autoStartOn);
        }
        else
        {
            autoStartupOptions.check(R.id.autoStartOff);
        }

            autoStartupOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if(i==R.id.autoStartOn)
                    {
                        sessionManager.setAValue(setAutoStartUP,"YES");
                    }
                    else if(i==R.id.autoStartOff)
                    {
                        sessionManager.setAValue(setAutoStartUP,"NO");
                    }
                }
            });

            tune_title.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                     RadioButton the_selected_tune=(RadioButton)dialog.findViewById(radioGroup.getCheckedRadioButtonId());

                    int selected_track=R.raw.simply_ringtone;

                    try {

                        mediaPlayer.reset();


                     switch (the_selected_tune.getId())
                     {
                         case R.id.joker_killed_batman:

                             selected_track=R.raw.joker_killed_batman;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.angry_mom_yeling:

                             selected_track=R.raw.angry_mom_yelling;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.friend_waking_you_up:

                             selected_track=R.raw.friend_waking_you_up;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.screaming_idiot:

                             selected_track=R.raw.screaming_idiot;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.gollum_is_whispering:

                             selected_track=R.raw.gollum_is_whispering;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.lovecraft_cthulhu:

                             selected_track=R.raw.lovecrafts_cthulhu;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.starwarsstartwork:

                             selected_track=R.raw.star_wars_start_work;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;

                         case R.id.simplyringtone:

                             selected_track=R.raw.simply_ringtone;

                             sessionManager.setAValue(setAlarmKeySet,String.valueOf(the_selected_tune.getId()));

                             sessionManager.setAValue(getalarmringtone,String.valueOf(selected_track));

                             break;
                     }

                         mediaPlayer.setAudioStreamType(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)));
                         AssetFileDescriptor assetFileDescriptor = activity.getResources().openRawResourceFd(selected_track);
                         mediaPlayer.setDataSource(assetFileDescriptor);
                         mediaPlayer.prepare();
                         mediaPlayer.start();
                     }
                     catch (Exception e){
                         Toast.makeText(activity, "Song Selection Error"+activity.getResources().getString(selected_track), Toast.LENGTH_SHORT).show();
                     }



                }
            });


            volumeOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (i)
                    {
                        case R.id.mediaSelected:
                            sessionManager.setAValue(setVolumeOutputType,String.valueOf(AudioManager.STREAM_MUSIC));
                            break;
                        case R.id.ringSelected:
                            sessionManager.setAValue(setVolumeOutputType,String.valueOf(AudioManager.STREAM_RING));
                            break;
                        case R.id.alarmSelected:
                            sessionManager.setAValue(setVolumeOutputType,String.valueOf(AudioManager.STREAM_ALARM));
                            //Toast.makeText(activity, sessionManager.getValue(setVolumeOutputType), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });

            dialog.setCanceledOnTouchOutside(true);

            dialog.show();

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    mediaPlayer.reset();
                }
            });

        }


}
