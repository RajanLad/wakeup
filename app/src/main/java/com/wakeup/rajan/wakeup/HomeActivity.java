package com.wakeup.rajan.wakeup;

import android.app.Dialog;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wakeup.rajan.wakeup.CustomViews.CustomSettingsDialogView;
import com.wakeup.rajan.wakeup.CustomViews.CustomTimerSelectionDialogView;
import com.wakeup.rajan.wakeup.Functions.Functions;
import com.wakeup.rajan.wakeup.Functions.SessionManager;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.DecimalFormat;

public class HomeActivity extends AppCompatActivity {

    TextView timerDial,volumeValue,startCountdownButton;
    SessionManager sessionManager;
    AudioManager am;

    MediaPlayer mediaPlayer;

    CountDownTimer countDownTimer;

    String timerKeyValue="TIMEROPTIONS";
    String setVolumeOutputType="SETVOLUMETYPE";
    String getalarmringtone="GETALARMRINGTONE";
    String setAutoStartUP="SETAUTOSTARTUP";

    int volume_level;

    AssetFileDescriptor assetFileDescriptor;

    boolean hasCountdownStarted=false;

    CustomTimerSelectionDialogView customTimerSelectionDialogView;
    CustomSettingsDialogView customSettingsDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        timerDial=(TextView)findViewById(R.id.timerDial);
        volumeValue=(TextView)findViewById(R.id.volumeValue);
        startCountdownButton=(TextView)findViewById(R.id.startCountdownButton);

        sessionManager=new SessionManager(this);
        customTimerSelectionDialogView=new CustomTimerSelectionDialogView();

        mediaPlayer= new MediaPlayer();

        if(sessionManager.getValue(setAutoStartUP)!=null && sessionManager.getValue(setAutoStartUP).equals("YES"))
        {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    startCountdownButton.performClick();
                }
            }, 1000);
        }
        else
        {
            //future
        }


//        new CountDownTimer(a, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//                int remaingSec=(int)millisUntilFinished%60000;
//                timerDial.setText(new DecimalFormat("00").format(millisUntilFinished/60000)+" : "+new DecimalFormat("00").format(remaingSec/1000)+"");
//            }
//
//            public void onFinish() {
//                timerDial.setText("done!");
//            }
//        }.start();



        if(sessionManager.getValue(timerKeyValue)==null)
            timerDial.setText("01:00");
        else
            timerDial.setText(sessionManager.getValue(timerKeyValue));


        //initialize audio levels at HomeActivity
        am = (AudioManager) getSystemService(AUDIO_SERVICE);
        volume_level= am.getStreamVolume(AudioManager.STREAM_MUSIC);
        //volumeValue.setText(Functions.getTheMessage(am.getStreamVolume(6),am.getStreamMaxVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType))),this));

        if(sessionManager.getValue(setVolumeOutputType)==null)
            sessionManager.setAValue(setVolumeOutputType,String.valueOf(AudioManager.STREAM_MUSIC));

        getSupportActionBar().setElevation(0);


        timerDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(countDownTimer!=null)
                    countDownTimer.cancel();

                customTimerSelectionDialogView.showDialog(HomeActivity.this, "Error de conexión al servidor",sessionManager,timerDial);

            }
        });


        startCountdownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!hasCountdownStarted) {
                    hasCountdownStarted=true;
                    //Integer.parseInt(timerDial.getText().toString().split(":")[0]) * 6
                    countDownTimer = new CountDownTimer(Integer.parseInt(timerDial.getText().toString().split(":")[0]) * 60000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            int remaingSec = (int) millisUntilFinished % 60000;
                            timerDial.setText(new DecimalFormat("00").format(millisUntilFinished / 60000) + ":" + new DecimalFormat("00").format(remaingSec / 1000) + "");
                            startCountdownButton.setText(R.string.stop);
                            startCountdownButton.setBackgroundResource(R.drawable.circular_background_red);
                        }

                        public void onFinish() {
                            timerDial.setText("00:00");

                            startCountdownButton.setTextSize(20f);

                            startCountdownButton.setText(R.string.wakeupmessage);

                            try{
                            mediaPlayer.setAudioStreamType(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)));
                            if(sessionManager.getValue(getalarmringtone)!=null)
                                assetFileDescriptor = getResources().openRawResourceFd(Integer.parseInt(sessionManager.getValue(getalarmringtone)));
                            else if(sessionManager.getValue(getalarmringtone)==null)
                                sessionManager.setAValue(getalarmringtone,String.valueOf(R.raw.simply_ringtone));
                                assetFileDescriptor = getResources().openRawResourceFd(Integer.parseInt(sessionManager.getValue(getalarmringtone)));
                            mediaPlayer.setDataSource(assetFileDescriptor);
                            mediaPlayer.setLooping(true);
                            mediaPlayer.prepare();
                            mediaPlayer.start();
                            }
                            catch (IOException e)
                            {
                                Log.d("ERROR","While playing ringtone");
                            }
                        }
                    }.start();
                }
                else
                {
                    if(countDownTimer!=null) {
                        mediaPlayer.reset();
                        countDownTimer.cancel();
                        hasCountdownStarted=false;
                        startCountdownButton.setTextSize(30f);
                        startCountdownButton.setText(R.string.start);
                        startCountdownButton.setBackgroundResource(R.drawable.circular_background);
                    }
                    if(sessionManager.getValue(timerKeyValue)==null)
                        timerDial.setText("01:00");
                    else
                        timerDial.setText(sessionManager.getValue(timerKeyValue));
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP){
                am = (AudioManager) getSystemService(AUDIO_SERVICE);
                //am.setStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),0);
                am.adjustStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),
                    AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
                volumeValue.setText(Functions.getTheMessage(am.getStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType))),am.getStreamMaxVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType))),this,sessionManager.getValue(SessionManager.GENDER)));
            return super.onKeyDown(keyCode, event);
        }else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
                am = (AudioManager) getSystemService(AUDIO_SERVICE);
            //am.setStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),0);
                am.adjustStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType)),
                        AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
                //volumeValue.setText(am.getStreamVolume(AudioManager.STREAM_ALARM)+""+am.getStreamMaxVolume(AudioManager.STREAM_ALARM));
            volumeValue.setText(Functions.getTheMessage(am.getStreamVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType))),am.getStreamMaxVolume(Integer.parseInt(sessionManager.getValue(setVolumeOutputType))),this,sessionManager.getValue(SessionManager.GENDER)));
            return super.onKeyDown(keyCode, event);
        }else{
            // pass the key event onto the OS
            return super.onKeyDown(keyCode, event);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.menu_main_setting)
        {
            customSettingsDialogView=new CustomSettingsDialogView();

            customSettingsDialogView.showDialog(this, "Error de conexión al servidor",sessionManager,mediaPlayer);

            //Toast.makeText(HomeActivity.this, "GoodGal", Toast.LENGTH_SHORT).show();
        }
            return super.onOptionsItemSelected(item);

    }


}
