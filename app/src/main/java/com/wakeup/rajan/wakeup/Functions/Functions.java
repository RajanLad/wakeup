package com.wakeup.rajan.wakeup.Functions;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.wakeup.rajan.wakeup.HomeActivity;
import com.wakeup.rajan.wakeup.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Functions
{

    public static boolean isMobileValid(String mobile) {
        boolean isValid = false;

        if (mobile.length()==10)
        {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isMobileValidWithCode(String mobile) {
        boolean isValid = false;

        if (mobile.startsWith("+") && mobile.length()>11)
        {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isEmpty(String value) {
        boolean isValid = false;

        if (!value.isEmpty())
        {
            isValid = true;
        }
        return isValid;
    }


    public static int getTheMessage(int current_value,int max_value,Activity activity,String gender) {
        if(gender.equals("GUY")) {
            if ((((float) current_value / (float) max_value) * 100) <= 0f)
                return R.string.zeropercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 10f)
                return R.string.tenpercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 20f)
                return R.string.twentypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 30f)
                return R.string.thirtypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 40f)
                return R.string.fortypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 50f)
                return R.string.fiftypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 60f)
                return R.string.sixtypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 70f)
                return R.string.seventypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 80f)
                return R.string.eightypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 90f)
                return R.string.ninetypercent_m;
            else if ((((float) current_value / (float) max_value) * 100) <= 100f)
                return R.string.hundredpercent_m;
            else
                return (current_value / max_value);
        }
        else
        {
            if ((((float) current_value / (float) max_value) * 100) <= 0f)
                return R.string.zeropercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 10f)
                return R.string.tenpercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 20f)
                return R.string.twentypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 30f)
                return R.string.thirtypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 40f)
                return R.string.fortypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 50f)
                return R.string.fiftypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 60f)
                return R.string.sixtypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 70f)
                return R.string.seventypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 80f)
                return R.string.eightypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 90f)
                return R.string.ninetypercent_f;
            else if ((((float) current_value / (float) max_value) * 100) <= 100f)
                return R.string.hundredpercent_f;
            else
                return (current_value / max_value);
        }
    }

}

