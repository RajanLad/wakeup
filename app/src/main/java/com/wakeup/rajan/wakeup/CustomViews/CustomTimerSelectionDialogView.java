package com.wakeup.rajan.wakeup.CustomViews;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wakeup.rajan.wakeup.Functions.SessionManager;
import com.wakeup.rajan.wakeup.R;

public class CustomTimerSelectionDialogView {

    TextView one_minute,two_minute,five_minute,
            ten_minute,fifteen_minute,twenty_minute,
            twenty_five_minute,thirty_minute,thirty_five_minute,
            forty_minute,fifty_minute,sixty_minute;

    String timeKeySet="TIMEROPTIONS";


    public String showDialog(final Activity activity, String msg, final SessionManager sessionManager, final TextView timerDial){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_timer_selection_dialog_view);

        one_minute=(TextView)dialog.findViewById(R.id.one_minute);
        two_minute=(TextView)dialog.findViewById(R.id.two_minute);
        five_minute=(TextView)dialog.findViewById(R.id.five_minute);
        ten_minute=(TextView)dialog.findViewById(R.id.ten_minute);
        fifteen_minute=(TextView)dialog.findViewById(R.id.fifteen_minute);
        twenty_minute=(TextView)dialog.findViewById(R.id.twenty_minute);
        twenty_five_minute=(TextView)dialog.findViewById(R.id.twenty_five_minute);
        thirty_minute=(TextView)dialog.findViewById(R.id.thirty_minute);
        thirty_five_minute=(TextView)dialog.findViewById(R.id.thirty_five_minute);
        forty_minute=(TextView)dialog.findViewById(R.id.forty_minute);
        fifty_minute=(TextView)dialog.findViewById(R.id.fifty_minute);
        sixty_minute=(TextView)dialog.findViewById(R.id.sixty_minute);


        one_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"01:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        two_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"02:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        five_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"05:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        ten_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"10:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        fifteen_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"15:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        twenty_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"20:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        twenty_five_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"25:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        thirty_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"30:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        thirty_five_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"35:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        forty_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"40:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        fifty_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"50:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        sixty_minute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setAValue(timeKeySet,"60:00");

                timerDial.setText(sessionManager.getValue(timeKeySet));

                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);

        dialog.show();

        return sessionManager.getValue(timeKeySet);
    }
}
