package com.wakeup.rajan.wakeup.Functions;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.wakeup.rajan.wakeup.MainActivity;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rajan on 12/9/2016.
 */

public class SessionManager
{
    SharedPreferences wakeUpSession;

    Editor edit;

    Context context;

    int PRIVATE_MODE=0;

    // Sharedpref file name
    private static final String SESSION_NAME = "WAKEUPSESSION";

    // All Shared Preferences Keys
    private static final String HAS_AGREEMENT = "IsLoggedIn";

    public static final String GENDER = "Gender";


    List<String> allKeys=new ArrayList();

    // Constructor
    public SessionManager(Context context){
        this.context = context;
        wakeUpSession = this.context.getSharedPreferences(SESSION_NAME, PRIVATE_MODE);
        edit = wakeUpSession.edit();
    }

    /*
    *
    *
    * check login status if true then jump else stay on same page
    * */


    /**
     * Create login session
     * */
    public void createSession(String gender){
        // Storing login value as TRUE
        edit.putBoolean(HAS_AGREEMENT, true);

        // Storing name in pref
        edit.putString(GENDER, gender);


        // commit changes
        edit.commit();
    }
    /*
    Store other related user details
    * */


    /**/

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */

    public void setAValue(String key,String value)
    {
        allKeys.add(key);

        edit.putString(key, value);

        edit.commit();
    }

    public String getValue(String key)
    {
        return  wakeUpSession.getString(key, null);
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(GENDER, wakeUpSession.getString(GENDER, null));


        // return user
        return user;
    }

    /**
     * Clear session details
     * */


    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return wakeUpSession.getBoolean(HAS_AGREEMENT, false);
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }



}
