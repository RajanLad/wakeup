package com.wakeup.rajan.wakeup;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wakeup.rajan.wakeup.Functions.SessionManager;
import com.wakeup.rajan.wakeup.JavaClasses.GIFImageView;

public class MainActivity extends AppCompatActivity {

    TextView genderSelectionGuy,genderSelectionGal,termsAndConditionsPost;
    CheckBox swearOption,termsAndCondition;
    Button proceedAhead;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager=new SessionManager(this);

        if(sessionManager.isLoggedIn())
        {
            Intent intent=new Intent(MainActivity.this,SplashActivity.class);

            startActivity(intent);

            // Closing all the Activities
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            finish();

        }

        //Sleeping smiley emoji
        GIFImageView gifImageView = (GIFImageView) findViewById(R.id.gifsleepicon);
        gifImageView.setGifImageResource(R.drawable.sleep_08);



        genderSelectionGal=(TextView)findViewById(R.id.femaleoption);
        genderSelectionGuy=(TextView)findViewById(R.id.maleoption);
        termsAndConditionsPost=(TextView)findViewById(R.id.termsandcondition_post);

        termsAndCondition=(CheckBox)findViewById(R.id.termsandcondition);

        proceedAhead=(Button)findViewById(R.id.proceedButton);



        termsAndConditionsPost.setText(Html.fromHtml("<a href='http://rajanlabs.000webhostapp.com/WakeUpApp/terms.php' > Terms and Conditions</a>"));
        termsAndConditionsPost.setMovementMethod(LinkMovementMethod.getInstance());

        genderSelectionGuy.setTextColor(Color.RED);

        termsAndCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    proceedAhead.setText(getResources().getText(R.string.button_proceed));
                    proceedAhead.setEnabled(true);
                }
                else
                {
                    proceedAhead.setText(getResources().getText(R.string.button_note));
                    proceedAhead.setEnabled(false);
                }
            }
        });
        proceedAhead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if(Math.abs(genderSelectionGal.getCurrentTextColor())==65536)
                    {
                        sessionManager.createSession("GAL");

                        Intent intent=new Intent(MainActivity.this,SplashActivity.class);

                        // Closing all the Activities
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        // Add new Flag to start new Activity
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        startActivity(intent);

                        finish();
                    }
                else if(Math.abs(genderSelectionGuy.getCurrentTextColor())==65536)
                {
                    sessionManager.createSession("GUY");

                    Intent intent=new Intent(MainActivity.this,SplashActivity.class);

                    // Closing all the Activities
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    // Add new Flag to start new Activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);

                    finish();
                }
            }
        });

        genderSelectionGuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderSelectionGuy.setTextColor(Color.RED);
                genderSelectionGal.setTextColor(Color.BLACK);
            }
        });

        genderSelectionGal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderSelectionGuy.setTextColor(Color.BLACK);
                genderSelectionGal.setTextColor(Color.RED);
            }
        });




    }
}
